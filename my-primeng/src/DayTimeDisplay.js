import React from 'react';
import { Box } from '@mui/material';

const DateTimeDisplay = ({ value, type}) => {
  return (
    <Box sx={{textAlign:'center'}}>
      <Box sx={{m:'10px'}}>{value}{type}</Box>
    </Box>
  );
};

export default DateTimeDisplay;
