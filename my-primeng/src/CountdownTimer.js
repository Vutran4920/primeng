import React from 'react';
import DateTimeDisplay from './DayTimeDisplay';
import { useCountdown } from './Hooks/useCountdown';
import { Box } from '@mui/material';

const ExpiredNotice = () => {
  return (
    <div className="expired-notice">
      <span>Expired!!!</span>
      <p>Please select a future date and time.</p>
    </div>
  );
};

const ShowCounter = ({  hours, minutes, seconds }) => {
  return (
    <Box sx={{
      display: 'flex',
      p: '12px', m: 'auto', 
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center',
    }}>
      <DateTimeDisplay value={hours} type={'h'} isDanger={false} />  
      <DateTimeDisplay value={minutes} type={'m'} isDanger={minutes <= 15} />
      <DateTimeDisplay value={seconds} type={'s'} isDanger={false} /> 
    </Box>
  );
};

const CountdownTimer = ({ targetDate }) => {
  const [ hours, minutes, seconds] = useCountdown(targetDate);

  if ( hours + minutes + seconds <= 0) {
    return <ExpiredNotice />;
  } else {
    return (
      <ShowCounter
        hours={hours}
        minutes={minutes}
        seconds={seconds}
      />
    );
  }
};

export default CountdownTimer;
