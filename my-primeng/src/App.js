import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./views/LoginPage/Loginpage";
import TestView from "./views/TeacherPage/TestView";
import { LessonView, CreateLesson } from "./views/TeacherPage";
import userData from "./views/Admin/userData";
import studentProfile from "./views/StudentPage/studentProfile";
import studentTestView from "./views/StudentPage/StdTestView";
import {
  studentDuringTest,
  studentPreTest,
  studentAfterTest,
  studentLessonView,
} from "./views/StudentPage";
import HandelTextQuestion from "./views/TeacherPage/CreateTest";
import teacherProfile from "./views/TeacherPage/teacherProfile";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<LoginPage />}></Route>
        <Route path="/teacher/testview" element={<TestView />}></Route>
        <Route path="/teacher/createlesson" element={<CreateLesson />}></Route>
        <Route path="/teacher/lessonview" element={<LessonView />}></Route>
        <Route
          path="/teacher/createtest"
          Component={HandelTextQuestion}
        ></Route>
        <Route path="/teacher/profile" Component={teacherProfile}></Route>
        <Route path="/admin" Component={userData}></Route>
        <Route path="/student/profile" Component={studentProfile}></Route>
        <Route path="/student/testview" Component={studentTestView}></Route>
        <Route path="/student/lessonview" Component={studentLessonView}></Route>
        <Route path="/student/pretest" Component={studentPreTest}></Route>
        <Route path="/student/duringtest" Component={studentDuringTest}></Route>
        <Route path="/student/aftertest" Component={studentAfterTest}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
