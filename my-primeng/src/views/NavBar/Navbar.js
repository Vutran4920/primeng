import Toolbar from "@mui/material/Toolbar";
import AppBar from "@mui/material/AppBar";
import React from "react";
import SchoolIcon from "@mui/icons-material/School";
import IconButton from "@mui/material/IconButton";
import { Box } from "@mui/material";

const NavBar = (props) => {
  return (
    <Box sx={{ mb: "64px" }}>
      <Box>
        <AppBar sx={{ backgroundColor: "#339999" }}>
          <Toolbar>
            <Box
              sx={{
                flexGrow: 2,
                textAlign: "left",
                color: "aliceblue",
                fontSize: "30px",
                fontWeight: "700",
              }}
            >
              Primeng{" "}
            </Box>
            <Box sx={{ p: "16px", fontSize: "20px", fontWeight: "600" }}>
              {" "}
              {props.role1}{" "}
            </Box>
            <Box sx={{ p: "16px", fontSize: "20px", fontWeight: "600" }}>
              {" "}
              {props.role2}{" "}
            </Box>
            <Box sx={{ p: "16px", fontSize: "20px", fontWeight: "600" }}>
              {" "}
              {props.role3}{" "}
            </Box>
            <Box sx={{ p: "16px", fontSize: "20px", fontWeight: "600" }}>
              {" "}
              {props.role4}{" "}
            </Box>
            <Box sx={{ p: "16px", fontSize: "20px", fontWeight: "600" }}>
              {" "}
              {props.role5}{" "}
            </Box>
            <IconButton sx={{ backgroundColor: "#339999" }}>
              <SchoolIcon
                sx={{
                  mr: 1,
                  flexGrow: 1,
                  textAlign: "right",
                  fontSize: "xx-large",
                  color: "white",
                  p: "8px",
                }}
              />
            </IconButton>
          </Toolbar>
        </AppBar>
      </Box>
    </Box>
  );
};
export default NavBar;
