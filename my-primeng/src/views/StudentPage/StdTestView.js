import React from "react";
import { Box } from "@mui/material";
import NavBar from "../NavBar/Navbar";
import TestCard from "../../component/TestCard";
import TestCardScore from "../../component/TestCardScore";
import Footer from "../Footer/Footer";

const STestView = () => {
  return (
    <Box sx={{ backgroundColor: "#EEEEEE" }}>
      <NavBar
        role1="Bài học"
        role2="Xem các bài kiểm tra"
        role3="Thông tin học sinh"
      />
      <Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            mb: "16px",
          }}
        >
          <Box
            sx={{
              textAlign: "center",
              fontSize: "50px",
              fontWeight: "700",
              lineHeight: 2,
              mb: "30px",
              mt: "12px",
              textTransform: "uppercase",
              color: "#526D82",
            }}
          >
            {" "}
            Danh sách các bài kiểm tra
          </Box>
          <Box
            sx={{
              textAlign: "left",
              fontSize: "40px",
              fontWeight: "600",
              width: "max-content",
              borderBottom: "1px solid black",
              p: "12px",
              mb: "8px",
              color: "#526D82",
            }}
          >
            Các bài kiểm tra khả dụng
          </Box>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              mt: "15px",
              gap: "51px",
              p: "12px",
              mb: "20px",

              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <TestCard
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="40 câu"
            />
            <TestCard
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="40 câu"
            />
            <TestCard
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="40 câu"
            />
            <TestCard
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="40 câu"
            />
            <TestCard
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="40 câu"
            />
          </Box>
          <Box
            sx={{
              textAlign: "left",
              fontSize: "40px",
              fontWeight: "600",
              width: "max-content",
              borderBottom: "1px solid black",
              p: "12px",
              mb: "8px",
              color: "#526D82",
            }}
          >
            Các bài kiểm tra đã làm
          </Box>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              mt: "15px",
              gap: "51px",
              p: "12px",
              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <TestCardScore
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="36/40 câu"
              score="9.0"
            />
            <TestCardScore
              testname="Speaking Test321321321321312"
              testcode="CV2345"
              testtime="15 phút"
              questionquan="3 phần"
              score="7.5"
            />
            <TestCardScore
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="36/40 câu"
              score="9.0"
            />
            <TestCardScore
              testname="Speaking Test321321321321312"
              testcode="CV2345"
              testtime="15 phút"
              questionquan="3 phần"
              score="7.5"
            />
            <TestCardScore
              testname="Listening Test"
              testcode="CV2345"
              testtime="90 phút"
              questionquan="36/40 câu"
              score="9.0"
            />
          </Box>
        </Box>
      </Box>
      <Box>
        <Footer />
      </Box>
    </Box>
  );
};
export default STestView;
