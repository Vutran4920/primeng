export { default as studentProfile} from './studentProfile';
export { default as studentTestView} from './StdTestView';
export { default as studentLessonView} from './StdLessonView';
export { default as studentPreTest} from './StdPreTest';
export { default as studentDuringTest} from './StdDurTest';
export { default as studentAfterTest} from './StdAfterTest';