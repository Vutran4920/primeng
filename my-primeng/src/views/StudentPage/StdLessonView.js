import React from "react";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import { Box, List, ListItem } from "@mui/material";
import AutoStoriesIcon from "@mui/icons-material/AutoStories";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
const StdLessonView = () => {
  return (
    <Box
      sx={{
        backgroundColor: "#EEEEEE",
      }}
    >
      <Box>
        <NavBar role3="Xem bài học" role4="Xem kiểm tra" />
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          p: "2px",
        }}
      >
        <Box
          sx={{
            mt: "30px",
            width: "300px",
            position: "relative",
          }}
        >
          <List
            sx={{
              position: "fixed",
              left: "8px",
              top: "80px",
              marginTop: 3,
              backgroundColor: "#7B8FA1",
              width: "200px",
              color: "white",
              height: "max-content",
              zIndex: 3,
              borderRadius: 2,
              padding: 1,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <ListItem
              sx={{
                display: "flex",
                width: "100%",
                height: "50px",
                borderRadius: "10px",
                marginTop: "20px",

                "&:hover": {
                  backgroundColor: "#FFFFFF",
                  opacity: 0.4,
                  color: "black",
                  cursor: "pointer",
                  transition: "all 0.2s linear",
                },
              }}
            >
              <AutoStoriesIcon sx={{ marginRight: 2 }} />
              Lớp 1
            </ListItem>
            <ListItem
              sx={{
                display: "flex",
                width: "100%",
                height: "50px",
                borderRadius: "10px",
                "&:hover": {
                  backgroundColor: "#FFFFFF",
                  opacity: 0.4,
                  color: "black",
                  cursor: "pointer",
                  transition: "all 0.4s linear",
                },
              }}
            >
              <AutoStoriesIcon sx={{ marginRight: 2 }} />
              Lớp 2
            </ListItem>
            <ListItem
              sx={{
                display: "flex",
                width: "100%",
                height: "50px",
                borderRadius: "10px",
                "&:hover": {
                  backgroundColor: "#FFFFFF",
                  opacity: 0.4,
                  color: "black",
                  cursor: "pointer",
                  transition: "all 0.4s linear",
                },
              }}
            >
              <AutoStoriesIcon sx={{ marginRight: 2 }} />
              Lớp 3
            </ListItem>
            <ListItem
              sx={{
                display: "flex",
                width: "100%",
                height: "50px",
                borderRadius: "10px",
                "&:hover": {
                  backgroundColor: "#FFFFFF",
                  opacity: 0.4,
                  color: "black",
                  cursor: "pointer",
                  transition: "all 0.4s linear",
                },
              }}
            >
              <AutoStoriesIcon sx={{ marginRight: 2 }} />
              Lớp 4
            </ListItem>
            <ListItem
              sx={{
                display: "flex",
                width: "100%",
                height: "50px",
                borderRadius: "10px",
                "&:hover": {
                  backgroundColor: "#FFFFFF",
                  opacity: 0.4,
                  color: "black",
                  cursor: "pointer",
                  transition: "all 0.4s linear",
                },
              }}
            >
              <AutoStoriesIcon sx={{ marginRight: 2 }} />
              Lớp 5
            </ListItem>
          </List>
        </Box>
        <Box sx={{ mt: "30px", flex: 5 }}>
          <Box
            sx={{
              textAlign: "center",
              fontSize: "50px",
              fontWeight: "700",
              mb: "8px",
            }}
          >
            {" "}
            Danh sách các bài học{" "}
          </Box>
          <Box
            sx={{
              width: "auto",
              p: "20px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                gap: "20px",
              }}
            >
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
              <Box
                sx={{
                  border: "none",
                }}
              >
                <Card
                  sx={{
                    backgroundColor: "#16a085",
                    border: "1px groove #DDDDDD",
                    p: "16px",
                    mb: "8px",
                    width: "315px",
                    height: "100px",
                    borderRadius: "12px",
                    color: "#EEEEEE",
                    boder: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CardContent
                    sx={{
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    Speaking English Class1
                  </CardContent>
                </Card>
                <Box
                  sx={{ fontWeight: "500", color: "#526D82", fontSize: "18px" }}
                >
                  Giáo Viên: Vương Gia Hào
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box>
        <Footer />
      </Box>
    </Box>
  );
};
export default StdLessonView;
