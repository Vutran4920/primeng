import React from "react";
import { Box } from "@mui/material";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import Link from '@mui/material/Link';

const SAfTest = () => {
    return (
        <Box sx={{backgroundColor:'#EEEEEE'}}>
            <NavBar
                role1='Bài học'
                role2='Xem các bài kiểm tra'
                role3="Thông tin học sinh" />
            <Box>
                <Box sx={{
                    textAlign: 'start',
                    p: '20px',
                    m: 'auto',
                    fontSize:'20px',
                    fontWeight:'600'
                }}>
                    <Link color="#006666" underline="hover" href="/student/testview" sx={{p:'12px'}}>Quay về trang xem bài kiểm tra</Link>
                </Box>
                <Box sx={{
                    textAlign: 'center',
                    mt: '30px',
                    p: '30px',
                    height: '800px'
                }}>
                    <Box sx={{
                        fontSize: '50px',
                        textTransform: 'uppercase',
                        fontWeight: '600',
                        p: '20px'
                    }}>
                        Speaking Test 123
                    </Box>
                    <Box sx={{
                        fontSize: '40px',
                        fontWeight: '600',
                        pb: '20px',
                    }}>Test Code: ehehe1</Box>
                    <Box sx={{
                        fontSize: '30px',
                        pb: '12px'
                    }}>
                        Chúc mừng bạn đã vượt qua bài thi!
                    </Box>
                    <Box sx={{
                        fontSize: '30px',
                        pb: '12px'
                    }}>
                        Bạn đã làm đúng được 18/20 câu.
                    </Box>
                    <Box sx={{
                        p: '30px'
                        , m: 'auto', backgroundColor: '#00FF99',
                        color: 'white',
                        fontSize: '30px',
                        fontWeight: '600',
                    }}>Điểm số của bạn là: 9.0</Box>

                </Box>
            </Box>
            <Footer />
        </Box>
    );
}
export default SAfTest;