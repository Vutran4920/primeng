import React from "react";
import { Box } from "@mui/material";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import Test from "../../component/Test";



const SDurTest = () => {
    return (
        <Box sx={{ backgroundColor: '#EEEEEE' }}>
            <NavBar
                role1='Bài học'
                role2='Xem các bài kiểm tra'
                role3="Thông tin học sinh"
            />
            <Box>
                <Box sx={{
                    m:'20px',
                }}>
                    <Test />
                </Box>
            
            </Box>
            <Footer />
        </Box>
    );
}
export default SDurTest;