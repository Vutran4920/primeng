import { Box, Button } from "@mui/material";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";

const SPreTest = () => {
    return (
        <Box sx={{ backgroundColor: '#EEEEEE' }}>
            <NavBar
                role1='Bài học'
                role2='Xem các bài kiểm tra'
                role3="Thông tin học sinh"
            />
            <Box sx={{
                textAlign: 'center',
                mt: '30px',
                p: '12px',
                height: '800px'
            }}>
                <Box sx={{
                    fontSize: '50px',
                    textTransform: 'uppercase',
                    fontWeight: '600',
                    p: '8px'
                }}>
                    Speaking Test 123
                </Box>
                <Box sx={{
                    fontSize: '40px',
                    fontWeight:'600',
                    pb:'8px',
                }}>Test Code: ehehe1</Box>
                <Box sx={{
                    fontSize: '30px',
                    pb:'8px'
                }}>
                    Bài thi lần này có 3 phần. Học sinh sẽ làm trong vòng 90 phút. Học sinh đọc kĩ yêu cầu đề bài tránh bị sai xót và lạc đề. 
                </Box>
                <Box sx={{
                    fontSize: '30px',
                    pb:'8px'}}>
                    Trong lúc làm bài vui lòng không sử dụng tài liệu. Nếu phát hiện sẽ bị xử phạt và cảnh cáo.
                    Chúc các bạn kiểm tra tốt !
                    </Box>
                <Box sx={{
                    p:'20px'
                }}>
                    <Button sx={{
                        backgroundColor:'red',
                        color:'white',
                        p:'15px',
                        fontSize:'15px',
                        fontWeight:'700',
                        margin:'12px',
                        '&:hover':{
                            backgroundColor:'#CC0000'
                        }
                    }}> Quay lại </Button>
                    <Button sx={{
                        backgroundColor:'#33CC33',
                        color:'white',
                        p:'15px',
                        fontSize:'15px',
                        fontWeight:'700',
                        margin:'12px',
                        '&:hover':{
                            backgroundColor:'#00CC99'
                        }
                    }}> Bắt đầu </Button>
                </Box>
            </Box>
            <Footer />
        </Box>
    );
}
export default SPreTest;