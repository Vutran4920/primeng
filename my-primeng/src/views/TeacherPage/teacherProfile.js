import React from "react";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import { Box } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import Grid from "@mui/material/Grid";
const teacherProfile = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#EEEEEE",
      }}
    >
      <NavBar role3="Xem các bài học" role4="Xem các bài kiểm tra" />
      <Box
        sx={{
          margin: "5px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          width: "60vw",
          borderRadius: "15px",
          height: "600px",
          flexDirection: "column",
          p: "30px",
        }}
      >
        <Box
          sx={{
            position: "relative",
            display: "flex",
            width: "100%",
            height: "20%",
            flexDirection: "column",
            padding: "20px",
            alignItems: "center",
            justifyContent: "center",
            boxSizing: "border-box",
            borderRadius: "15px 15px 0 0",
            background: "#16a085",
          }}
        >
          <Avatar
            src="https://lh6.googleusercontent.com/-B3AYKH-PCgY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckyggzgJd-2OLwLGEJWCmV6vLQV-g/photo.jpg"
            alt=""
            sx={{
              position: " absolute",
              border: "5px solid #F1F6F9",
              bottom: "-80%",
              left: "20px",
              width: "150px",
              height: "150px",
              borderRadius: "50%",
            }}
          />
        </Box>
        <Box
          sx={{
            flexGrow: 8,
            display: "flex",
            flex: 2,
            background: "#F1F6F9",
            borderRadius: "0 0 15px 15px",
            width: "100%",
            flexDirection: "column",
          }}
        >
          <Box
            sx={{
              fontWeight: "bold",
              fontSize: "40px",
              marginLeft: "230px",
            }}
          >
            Nguyễn Minh Luân
          </Box>
          <Box
            sx={{
              display: "flex",
              wrap: "30px",
              padding: "30px",
              marginTop: "80px",
            }}
          >
            <Grid container spacing={2}>
              <Grid
                item
                xs={6}
                sx={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  p: "30px",
                }}
              >
                Tên Tài khoản : luan1230
              </Grid>
              <Grid
                item
                xs={6}
                sx={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  p: "30px",
                }}
              >
                Lớp : 123
              </Grid>
              <Grid
                item
                xs={6}
                sx={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  p: "30px",
                }}
              >
                Chức Danh : Giáo Viên
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};
export default teacherProfile;
