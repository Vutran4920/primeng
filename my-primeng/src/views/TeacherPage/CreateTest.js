import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import Button from "@mui/material/Button";
import { Box } from "@mui/material";
import React from "react";
import { TextField, FormControl } from "@mui/material";
import ImageIcon from "@mui/icons-material/Image";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import ClearIcon from "@mui/icons-material/Clear";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import AddBoxRoundedIcon from "@mui/icons-material/AddBoxRounded";
const HandelTextQuestion = () => {
  return (
    <Box
      sx={{
        backgroundColor: "#EEEEEE",

        width: "100vw",
        height: "100%",
        boxSizing: "border-box",
      }}
    >
      <Box>
        <NavBar
          role3="Bài học"
          role1="Xây dựng bài học"
          role2="Tạo kiểm tra"
          role4="Xem bài kiểm tra"
          dditem1="Thông tin tài khoản"
          dditem2="Tạo tài khoản cho học sinh"
        />
      </Box>
      <Box sx={{}}>
        <form
          style={{
            display: "flex",

            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <Box
            sx={{
              width: "600px",

              background: "white",
              padding: 3,
              margin: "40px 16px 16px 16px",
              borderRadius: 2,
            }}
          >
            <FormControl fullWidth sx={{ marginBottom: "20px" }}>
              <TextField
                label="Tiêu đề"
                sx={{ fontWeight: "Bold" }}
                inputProps={{ style: { fontSize: 40 } }}
              />
            </FormControl>
            <FormControl fullWidth sx={{}}>
              <TextField label="Mô tả" />
            </FormControl>
          </Box>
          <Box
            sx={{
              width: "600px",
              background: "white",
              padding: 3,
              margin: 2,
              borderRadius: 2,
              position: "relative",
            }}
          >
            <Box sx={{ position: "absolute", top: "-5px", right: "-80px" }}>
              <AddBoxRoundedIcon sx={{ color: "grey", fontSize: "60px" }} />
            </Box>
            <FormControl
              fullWidth
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <TextField
                id="standard-basic"
                label="Viết Câu hỏi ?"
                variant="standard"
                sx={{
                  flexGrow: 11,
                }}
              />
              <Box sx={{ flexGrow: 1, padding: 1, marginLeft: "50px" }}>
                <ImageIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatBoldIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatItalicIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatUnderlinedIcon sx={{ marginTop: 2, color: "grey" }} />
              </Box>
            </FormControl>
            <FormControl fullWidth>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: 2,
                }}
              >
                <TextField
                  id="standard-basic"
                  label="Lựa chọn 1"
                  variant="standard"
                  sx={{ flexGrow: 11 }}
                />
                <ClearIcon
                  sx={{ color: "grey", flexGrow: 1, padding: 1, marginTop: 3 }}
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: 2,
                }}
              >
                <TextField
                  id="standard-basic"
                  label="Lựa chọn 2"
                  variant="standard"
                  sx={{ flexGrow: 11 }}
                />
                <ClearIcon
                  sx={{ color: "grey", flexGrow: 1, padding: 1, marginTop: 3 }}
                />
              </Box>
              <Box sx={{ color: "grey", cursor: "pointer", marginTop: 2 }}>
                Thêm lựa chọn
              </Box>
            </FormControl>
          </Box>
          <Box
            sx={{
              width: "600px",
              background: "white",
              padding: 3,
              margin: 2,
              borderRadius: 2,
              position: "relative",
            }}
          >
            <FormControl
              fullWidth
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <TextField
                id="standard-basic"
                label="Viết Câu hỏi ?"
                variant="standard"
                sx={{
                  flexGrow: 11,
                }}
              />
              <Box sx={{ flexGrow: 1, padding: 1, marginLeft: "50px" }}>
                <ImageIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatBoldIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatItalicIcon sx={{ marginTop: 2, color: "grey" }} />
                <FormatUnderlinedIcon sx={{ marginTop: 2, color: "grey" }} />
              </Box>
            </FormControl>
            <FormControl fullWidth>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: 2,
                }}
              >
                <TextField
                  id="standard-basic"
                  label="Lựa chọn 1"
                  variant="standard"
                  sx={{ flexGrow: 11 }}
                />
                <ClearIcon
                  sx={{ color: "grey", flexGrow: 1, padding: 1, marginTop: 3 }}
                />
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: 2,
                }}
              >
                <TextField
                  id="standard-basic"
                  label="Lựa chọn 2"
                  variant="standard"
                  sx={{ flexGrow: 11 }}
                />
                <ClearIcon
                  sx={{ color: "grey", flexGrow: 1, padding: 1, marginTop: 3 }}
                />
              </Box>
              <Box sx={{ color: "grey", cursor: "pointer", marginTop: 2 }}>
                Thêm lựa chọn
              </Box>
            </FormControl>
          </Box>

          <Button
            variant="contained"
            sx={{
              width: "645px",
              margin: 2,
              boxSizing: "border-box",
              background: "#339999",
              "&:hover": {
                backgroundColor: "#339999",
              },
            }}
          >
            Tạo bài
          </Button>
        </form>
      </Box>
      <Box>
        <Footer />
      </Box>
    </Box>
  );
};
export default HandelTextQuestion;
