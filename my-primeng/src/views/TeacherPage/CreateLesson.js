import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import AddIcon from '@mui/icons-material/Add';
import { Box } from "@mui/material";

const CreateLesson = () => {
    return (
        <Box sx={{ backgroundColor: '#EEEEEE' }}>
            <Box>
                <NavBar
                    role3="Bài học"
                    role1="Xây dựng bài học"
                    role2="Tạo kiểm tra"
                    role4="Xem bài kiểm tra"
                    dditem1="Thông tin tài khoản"
                    dditem2="Tạo tài khoản cho học sinh"
                />
            </Box>
            <Box sx={{
                backgroundColor: 'white',
                width: '75%',
                height: '1000px',
                margin: 'auto'
            }}>
                <Box sx={{
                    textAlign: 'center',
                    p: '20px',
                    fontSize: '40px',
                    fontWeight: '600'
                }}> Xây dựng bài học </Box>
                <Box>
                    <AddIcon />

                </Box>
            </Box>
            <Box>
                <Footer />
            </Box>
        </Box>
    );
}
export default CreateLesson;