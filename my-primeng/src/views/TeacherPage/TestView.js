import NavBar from "../NavBar/Navbar";
import TestCard from "../../component/TestCard";
import Footer from "../Footer/Footer";
import { Box } from "@mui/material";

const TestView = () => {
  return (
    <Box sx={{ backgroundColor: "#EEEEEE", m: "-8px" }}>
      <Box>
        <NavBar
          role3="Bài học"
          role1="Xây dựng bài học"
          role2="Tạo kiểm tra"
          role4="Xem bài kiểm tra"
          dditem1="Thông tin tài khoản"
          dditem2="Tạo tài khoản"
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          mb: "16px",
        }}
      >
        <Box
          sx={{
            textAlign: "center",
            fontSize: "50px",
            fontWeight: "700",
            lineHeight: 2,
            fontFamily: "Helvetica",
            mb: "30px",
            mt: "12px",
            textTransform: "uppercase",
            color: "#526D82",
          }}
        >
          {" "}
          Các bài kiểm tra đã tạo{" "}
        </Box>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            mt: "15px",
            gap: "51px",
            p: 5,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <TestCard
            testname="Listening Test11111111111111111111111111"
            testcode="CV2345"
            testtime="90 phút"
            questionquan="40 câu"
            imageUrl="https://c0.wallpaperflare.com/preview/551/758/576/earphones-music-listen.jpg"
          />
          <TestCard
            testname="Reading test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://www.techexplorist.com/wp-content/uploads/2019/08/open-book.jpg"
          />
          <TestCard
            testname="Writing test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://img.freepik.com/free-photo/top-view-open-notepad-grey-surface_140725-88059.jpg?w=2000"
          />
          <TestCard
            testname="Speaking test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://iotcdn.oss-ap-southeast-1.aliyuncs.com/2020-11/ielts_speaking_mistakes_cue_card.jpg"
          />
          <TestCard
            testname="Listening Test11111111111111111111111111"
            testcode="CV2345"
            testtime="90 phút"
            questionquan="40 câu"
            imageUrl="https://c0.wallpaperflare.com/preview/551/758/576/earphones-music-listen.jpg"
          />
          <TestCard
            testname="Reading test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://www.techexplorist.com/wp-content/uploads/2019/08/open-book.jpg"
          />
          <TestCard
            testname="Writing test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://img.freepik.com/free-photo/top-view-open-notepad-grey-surface_140725-88059.jpg?w=2000"
          />
          <TestCard
            testname="Speaking test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://iotcdn.oss-ap-southeast-1.aliyuncs.com/2020-11/ielts_speaking_mistakes_cue_card.jpg"
          />
          <TestCard
            testname="Listening Test11111111111111111111111111"
            testcode="CV2345"
            testtime="90 phút"
            questionquan="40 câu"
            imageUrl="https://c0.wallpaperflare.com/preview/551/758/576/earphones-music-listen.jpg"
          />
          <TestCard
            testname="Reading test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://www.techexplorist.com/wp-content/uploads/2019/08/open-book.jpg"
          />
          <TestCard
            testname="Writing test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://img.freepik.com/free-photo/top-view-open-notepad-grey-surface_140725-88059.jpg?w=2000"
          />
          <TestCard
            testname="Speaking test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://iotcdn.oss-ap-southeast-1.aliyuncs.com/2020-11/ielts_speaking_mistakes_cue_card.jpg"
          />
          <TestCard
            testname="Listening Test11111111111111111111111111"
            testcode="CV2345"
            testtime="90 phút"
            questionquan="40 câu"
            imageUrl="https://c0.wallpaperflare.com/preview/551/758/576/earphones-music-listen.jpg"
          />
          <TestCard
            testname="Reading test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://www.techexplorist.com/wp-content/uploads/2019/08/open-book.jpg"
          />
          <TestCard
            testname="Writing test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://img.freepik.com/free-photo/top-view-open-notepad-grey-surface_140725-88059.jpg?w=2000"
          />
          <TestCard
            testname="Speaking test"
            testcode="CV458"
            testtime="45 phút"
            questionquan="30 câu"
            imageUrl="https://iotcdn.oss-ap-southeast-1.aliyuncs.com/2020-11/ielts_speaking_mistakes_cue_card.jpg"
          />
        </Box>
      </Box>
      <Box>
        <Footer />
      </Box>
    </Box>
  );
};
export default TestView;
