import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
const LoginPage = () => {
  const ddobject = {
    value1: "logout",
    value2: "null",
  };
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor:'#EEEEEE'
      }}
    >
      <div>
        <NavBar dditem1={ddobject.value1} dditem2={null} />
      </div>
      <div>
        <Box
          sx={{
            margin: 3,
            width: 400,
            height: 500,
            backgroundColor: "white",
            border:'1px solid grey',
            borderRadius: "8px",
          }}
        >
          <Box>
            <Typography
              variant="h5"
              sx={{
                color: "black",
                textAlign: "center",
                fontSize: "40px",
                padding: "40px 6px 6px 6px",
              }}
            >
              {" "}
              ĐĂNG NHẬP
            </Typography>
            <Stack
              spacing={2}
              sx={{
                padding: "12px",
              }}
            >
              <TextField
                id="username"
                label="Tên đăng nhập"
                type="text"
                autoComplete="current-username"
              />
              <TextField
                id="password"
                label="Mật khẩu"
                type="pasword"
                autoComplete="current-password"
              />
              <Button
                variant="contained"
                sx={{
                  background: "#339999",
                  "&:hover": {
                    backgroundColor: "#339999",
                  },
                }}
              >
                Đăng nhập
              </Button>
            </Stack>
          </Box>
        </Box>
      </div>
      <div>
        <Footer />
      </div>
    </Box>
  );
};
export default LoginPage;
