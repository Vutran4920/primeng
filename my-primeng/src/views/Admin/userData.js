import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";

import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import { grey } from "@mui/material/colors";
import NavBar from "../NavBar/Navbar";
import Footer from "../Footer/Footer";
import { Box } from "@mui/material";
function createUserData(id, fullName, userName, password, role, className) {
  return {
    id: id,
    fullName: fullName,
    userName: userName,
    password: password,
    role: role,
    className: className,
  };
}
const userRow = [
  createUserData(1, "Admin", "zedpro1230", "123", "admin", "không"),
  createUserData(
    2,
    "Nguyễn Minh Luân",
    "yasuoro1230",
    "321",
    "Giáo viên",
    "A4/2"
  ),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
  createUserData(3, "Vương Gia Hào", "1234546", "7894", "Học sinh", "A4/2"),
];
function AddUserdata() {
  const [open, setOpen] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState("teacher");

  const handleChange = (event) => {
    setSelectedOption(event.target.value);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ backgroundColor: "#EEEEEE" }}>
      <Button
        variant="contained"
        onClick={handleClickOpen}
        sx={{
          marginLeft: 2,
          marginTop: 2,
          background: "#339999",
          "&:hover": {
            backgroundColor: "#339999",
          },
        }}
      >
        Tạo tài khoản
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle sx={{ fontWeight: 600, fontSize: 30 }}>
          Tạo tài khoản mới
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Họ và tên"
            type="name"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Tên tài khoản"
            type="name"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Mật khẩu"
            type="password"
            fullWidth
            variant="standard"
          />
          <p style={{ color: grey }}>Phân Quyền</p>
          <Select value={selectedOption} onChange={handleChange}>
            <MenuItem value="teacher">Giáo viên</MenuItem>
            <MenuItem value="student">Học sinh</MenuItem>
          </Select>
          <TextField
            autoFocus
            margin="dense"
            id="className"
            label="Lớp"
            type="className"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Tạo</Button>
          <Button onClick={handleClose}>Thoát</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
function UpadateDataUser() {
  const [open, setOpen] = React.useState(false);
  const [role, setRole] = React.useState("");

  const handleChange = (event) => {
    setRole(event.target.value);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box>
      <EditIcon
        className="updateIcon"
        color="primary"
        sx={{ marginRight: 1 }}
        onClick={handleClickOpen}
      />
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Sửa đổi tài khoản</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Họ và tên"
            type="name"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Tên tài khoản"
            type="name"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Mật khẩu"
            type="password"
            fullWidth
            variant="standard"
          />
          <p style={{ color: grey }}>Phân Quyền</p>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={role}
            label="role"
            onChange={handleChange}
          >
            <MenuItem value="teacher">Giáo viên</MenuItem>
            <MenuItem value="student">Học Sinh</MenuItem>
          </Select>
          <TextField
            autoFocus
            margin="dense"
            id="className"
            label="Lớp"
            type="className"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Chỉnh Sửa</Button>
          <Button onClick={handleClose}>Thoát</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
function DeleteUser() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box>
      <DeleteIcon sx={{ color: "red" }} onClick={handleClickOpen} />
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle id="delete">{"Xóa tài khoản"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="deleteUser">
            Bạn có chắc muốn xóa tài khoản này
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Không</Button>
          <Button onClick={handleClose} autoFocus>
            Có
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
const userData = () => {
  return (
    <Box sx={{ background: "#EEEEEE", height: "100%" }}>
      <Box>
        <NavBar
          role1="Tạo bài học"
          role2="Xem các bài học"
          role3="Tạo bài kiểm tra"
          role4="Xem các bài kiểm tra"
        />
      </Box>
      <Box>
        <AddUserdata />
      </Box>
      <Box
        sx={{
          margin: " 16px 16px 60px 16px",
        }}
      >
        <TableContainer
          component={Paper}
          sx={{ width: 1, boxSizing: "border-box" }}
        >
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Họ và Tên</TableCell>
                <TableCell>Tên tài khoản</TableCell>
                <TableCell>Mật khẩu</TableCell>
                <TableCell>Quyền</TableCell>
                <TableCell>Lớp</TableCell>
                <TableCell>Hành động</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {userRow.map((row) => (
                <TableRow
                  key={row.id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.id}
                  </TableCell>
                  <TableCell>{row.fullName}</TableCell>
                  <TableCell>{row.userName}</TableCell>
                  <TableCell>{row.password}</TableCell>
                  <TableCell>{row.role}</TableCell>
                  <TableCell>{row.className}</TableCell>
                  <TableCell className="action" sx={{ display: "flex" }}>
                    <UpadateDataUser />
                    <DeleteUser />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <footer>
        <Footer />
      </footer>
    </Box>
  );
};

export default userData;
