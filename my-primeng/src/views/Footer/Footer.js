import { BottomNavigation } from "@mui/material";
import { Box } from '@mui/material';

const Footer = () =>{
    return(
        
        <Box>
            <BottomNavigation sx={{backgroundColor: '#EEEEEE',
                                borderTop: '1px solid black',
                                }}>
                    <Box className='text-center p-3'> 
                        <p>&copy; Copyright:{' '} Vu Tran and Luan Nguyen</p>
                    </Box>
            </BottomNavigation>
        </Box>
    );
}
export default Footer;