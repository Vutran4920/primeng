import React from "react";
import { Box } from "@mui/material";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";

import Typography from "@mui/material/Typography";
function TestCardScore(props) {
  return (
    <Card sx={{ maxWidth: 345, boxSizing: "border-box" }}>
      <CardMedia
        sx={{
          height: 60,
          background: "#66CC99",
          display: "flex",
          flexDirection: "column",
          padding: 2,
          color: "white",
          justifyContent: "center",
        }}
      >
        <Typography
          sx={{
            display: "flex",
            justifyContent: "space-between",
            fontSize: "24px",
            fontWeight: "bold",
          }}
        >
          <Box
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              width: "345px",
            }}
          >
            {props.testname}
          </Box>
        </Typography>
        <Box
          sx={{
            fontSize: "20px",
          }}
        >
          Code : {props.testcode}
        </Box>
      </CardMedia>
      <CardContent>
        <Typography
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#526D82",
          }}
        >
          <Box sx={{}}>
            <Box>Thời Gian : {props.testtime}</Box>
            <Box>Số Câu đúng : {props.questionquan}</Box>
          </Box>
        </Typography>
      </CardContent>
      <CardActions
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "start",
          m: "-8px",
          height: "50px",
        }}
      >
        <Typography
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "white",
            backgroundColor: "#66CC99",
            width: 1,
          }}
        >
          <Box sx={{ m: "16px" }}>Tổng Điểm : {props.score}</Box>
        </Typography>
      </CardActions>
    </Card>
  );
}
export default TestCardScore;
