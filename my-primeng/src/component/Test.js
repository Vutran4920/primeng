import React from "react";
import { Box, ImageList } from "@mui/material";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import CountdownTimer from '../CountdownTimer';
const Test = () => {
    const testime= 16*60*1000;
    const nowtime = new Date().getTime();
    const testrealtime = nowtime + testime;
    return (
        <Box sx={{
            backgroundColor: 'white',
            alignItems: 'center',
            borderRadius:'12px',
            mr:'12px',
            p:'30px'
        }}>
            <Box sx={{
                textAlign: 'center',
                p: '12px',
                mb: '20px'
            }}>
                <Box sx={{
                    fontSize: '60px',
                    textTransform: 'uppercase',
                    fontWeight: '600',
                    mt: '20px'
                }}>
                    Reading Test 1
                </Box>
                <Box sx={{ fontSize: '40px', p: '8px' }}>
                    Code: hehe12
                </Box>
                <Box sx={{ m:'auto',fontSize: '40px', p: '8px', border:'2px solid green', width:'fit-content'}}>
                    <CountdownTimer targetDate={testrealtime}/>
                </Box>
            </Box>
            <Box sx={{ p: '12px' }}>
                <Box>
                    <Box sx={{
                        fontSize: '35px',
                        fontWeight: '700',
                        pb: '12px'
                    }}>Choose the correct answer:</Box>
                    <Box>
                        <Box sx={{fontSize:'35px', mt:'12px'}}> Question 1: </Box>
                        <ImageList>
                            <img src="https://www.englishexercises.org/makeagame/my_documents/my_pictures/gallery/b/blue.jpg" alt="quest1" />
                        </ImageList>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="a" control={<Radio />} label="A. Red" />
                            <FormControlLabel value="b" control={<Radio />} label="B. Yellow" />
                            <FormControlLabel value="c" control={<Radio />} label="C. Blue" />
                        </RadioGroup>
                    </Box>
                    <Box>
                        <Box sx={{fontSize:'35px', mt:'12px'}}> Question 2: </Box>
                        <ImageList>
                            <img src="https://www.englishexercises.org/makeagame/my_documents/my_pictures/gallery/o/orange(colour).jpg" alt="quest1" />
                        </ImageList>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="a" control={<Radio />} label="A. Red" />
                            <FormControlLabel value="b" control={<Radio />} label="B. Yellow" />
                            <FormControlLabel value="c" control={<Radio />} label="C. Blue" />
                        </RadioGroup>
                    </Box>
                    <Box>
                        <Box sx={{fontSize:'35px', mt:'12px'}}> Question 3: How many dogs are there in the picture? </Box>
                        <ImageList>
                            <img src="https://www.englishexercises.org/makeagame/my_documents/my_pictures/2009/jul/147_images.jpg" alt="quest1" />
                        </ImageList>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="a" control={<Radio />} label="A. Red" />
                            <FormControlLabel value="b" control={<Radio />} label="B. Yellow" />
                            <FormControlLabel value="c" control={<Radio />} label="C. Blue" />
                        </RadioGroup>
                    </Box>
                    <Box>
                        <Box sx={{fontSize:'35px', mt:'12px'}}> Question 4: Which room is it? </Box>
                        <ImageList>
                            <img src="https://www.englishexercises.org/makeagame/my_documents/my_pictures/gallery/k/kitchen.jpg" alt="quest1" />
                        </ImageList>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="a" control={<Radio />} label="A. Kitchen" />
                            <FormControlLabel value="b" control={<Radio />} label="B. Bedroom" />
                            <FormControlLabel value="c" control={<Radio />} label="C. Hall" />
                        </RadioGroup>
                    </Box>
                    <Box>
                        <Box sx={{fontSize:'35px', mt:'12px'}}> Question 5: Which room is it? </Box>
                        <ImageList>
                            <img src="https://www.englishexercises.org/makeagame/my_documents/my_pictures/gallery/b/bedroom.jpg" alt="quest1" />
                        </ImageList>
                        <RadioGroup
                            aria-labelledby="demo-radio-buttons-group-label"
                            name="radio-buttons-group"
                        >
                            <FormControlLabel value="a" control={<Radio />} label="A. Bathroom" />
                            <FormControlLabel value="b" control={<Radio />} label="B. Bedroom" />
                            <FormControlLabel value="c" control={<Radio />} label="C. Living room" />
                        </RadioGroup>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}
export default Test;