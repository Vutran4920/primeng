import React from "react";
import Button from "@mui/material/Button";
import { Box } from "@mui/material";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import ListIcon from "@mui/icons-material/List";
import Typography from "@mui/material/Typography";
function TestCard(props) {
  return (
    <Card sx={{ maxWidth: 345, boxSizing: "border-box" }}>
      <CardMedia
        sx={{
          height: 60,
          background: "#66CC99",
          display: "flex",
          flexDirection: "column",
          padding: 2,
          color: "white",
          justifyContent: "center",
        }}
      >
        <Typography
          sx={{
            display: "flex",
            justifyContent: "space-between",
            fontSize: "24px",
            fontWeight: "bold",
          }}
        >
          <Box
            sx={{
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              width: "80%",
            }}
          >
            {props.testname}
          </Box>
          <Box
            sx={{
              width: "30px",
              height: "30px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer",
              borderRadius: "50%",
              "&:hover": {
                background: "grey",
                opacity: 0.7,
              },
            }}
          >
            <ListIcon />
          </Box>
        </Typography>
        <Box
          sx={{
            fontSize: "20px",
          }}
        >
          Code : {props.testcode}
        </Box>
      </CardMedia>
      <CardContent>
        <Typography
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#526D82",
          }}
        >
          <Box
            sx={{
              display: "-webkit-box",
              overflow: "hidden",
              textOverflow: "ellipsis",
              WebkitLineClamp: 3, // Number of lines to display
              WebkitBoxOrient: "vertical",
            }}
          >
            Bài kiểm tra kĩ năng nghe dành cho học sinh lớp 1A4 lấy điểm kiểm
            tra 15' . Các học sinh lớp khác có thể tham khảo.
          </Box>
          <Box sx={{}}>
            <Box>Thời Gian : {props.testtime}</Box>
            <Box>Số lượng câu hỏi : {props.questionquan}</Box>
          </Box>
        </Typography>
        <CardActions sx={{ p: 0, mt: 2 }}>
          <Button
            sx={{
              background: "#47A992",
              color: "white",
              "&:hover": {
                background: "#47A992",
                color: "#526D82",
              },
              fontSize: "16px",
              fontWeight: "600",
            }}
          >
            Làm Bài
          </Button>
        </CardActions>
      </CardContent>
    </Card>
  );
}
export default TestCard;
